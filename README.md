Unser Projekt heißt "4-Gewinnt".
Unser Programm stellt ein Spiel dar, welches für zwei Spieler geeignet ist.
Der Gewinner des Spiels ist derjenige, der als erstes 4 Spielsteine in einer
waagerechten, diagonalen oder senkrechten Linie gebracht hat.
Die größte technische Herrausforderung wird vermutlich das Platzieren der 
Spielsteine sein. Da das Programm nicht vorhandene Spielsteine überschreiben darf
und die Steinfarbe wechseln muss. Als Lösung denken wir uns eine Schleife, die die
Position der Steine bei jedem Zug überprüft.