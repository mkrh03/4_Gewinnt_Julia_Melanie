Zum Team gehören Melanie Krech und Julia Janky.
Wir sehen die Aufgabenteilung wie folgt vor:
Julia Janky wird die Spieloberfläche mit BoS programmieren. 
Dazu gehören das Spielfeld sowie die Spielsteine der zwei Spieler.
Außerdem wird sie die Spielanweisungen der einzelnen Spieler in das Programm einbauen.
Melanie Krech wird den Spielablauf programmieren.
Dies beinhaltet die Platzierung der Steine durch Anweisung und das Abwechseln der Spieler. 
Zudem integriert sie die Bedingungen für das Erlangen des Gewinnens in das Programm.